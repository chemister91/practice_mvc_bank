package homework.practice_mvc_bank.model;

public class Account {
    private Integer number;
    private Client client;
    private Double sum;
    private Boolean closed;

    public Integer getNumber() {
        return number;
    }

    public Account setNumber(Integer number) {
        this.number = number;
        return this;
    }

    public Client getClient() {
        return client;
    }

    public Account setClient(Client client) {
        this.client = client;
        return this;
    }

    public Double getSum() {
        return sum;
    }

    public Account setSum(Double sum) {
        this.sum = sum;
        return this;
    }

    public Account addSum(Double sum) {
        this.sum += sum;
        return this;
    }

    public Account withdrawSum(Double sum) {
        this.sum -= sum;
        return this;
    }

    public Boolean getClosed() {
        return closed;
    }

    public Account setClosed(Boolean closed) {
        this.closed = closed;
        return this;
    }
}
