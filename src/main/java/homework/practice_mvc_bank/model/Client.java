package homework.practice_mvc_bank.model;

import javax.validation.constraints.NotEmpty;
import java.util.Objects;

public class Client {

    private static final String NOT_EMPTY = "Поле не может быть пустым";

    @NotEmpty(message = NOT_EMPTY)
    private String fio;
    @NotEmpty(message = NOT_EMPTY)
    private String email;
    @NotEmpty(message = NOT_EMPTY)
    private String passHash;
    private String role;

    public Client() {
        this.role = "CLIENT";
    }

    public Client(String fio) {
        this.fio = fio;
    }

    public String getFio() {
        return fio;
    }

    public Client setFio(String fio) {
        this.fio = fio;
        return this;
    }

    public String getPassHash() {
        return passHash;
    }

    public Client setPassHash(String passHash) {
        this.passHash = passHash;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Client setEmail(String email) {
        this.email = email;
        return this;
    }

    public Integer getClientID() {
        return hashCode();
    }

    public String getRole() {
        return role;
    }

    public Client setRole(String role) {
        this.role = role;
        return this;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Client client = (Client) object;
        return email.equals(client.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email);
    }
}
