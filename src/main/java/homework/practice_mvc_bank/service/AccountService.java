package homework.practice_mvc_bank.service;

import homework.practice_mvc_bank.model.Account;
import homework.practice_mvc_bank.model.Client;

import java.util.List;

public interface AccountService {

    List<Account> getAllAccount();

    Boolean hasAccount(Client client);

    AccountService createAccount(String fio, String email, String passHash) throws IllegalAccessException;

    AccountService createAccount(Client client) throws IllegalAccessException;

    Account getAccountByEmail(String email);

    AccountService closeAccount(Integer accountNumber);

    AccountService openAccount(Integer accountNumber);

    AccountService replenishAccount(Integer accountNumber, Double sum);

    AccountService transferMoney(Integer fromAccountNumber, Integer toAccount, Double sum);
}
