package homework.practice_mvc_bank.service;

import homework.practice_mvc_bank.model.Client;
import homework.practice_mvc_bank.repos.ClientDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserDetailServiceImpl implements UserDetailsService {
    @Autowired
    private ClientDao clientDao;

    @Override
    public UserDetails loadUserByUsername(String email) {
        Client client = clientDao.getClientByEmail(email);
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(client.getRole()));
            return new User(client.getEmail(), client.getPassHash(), authorities);
    }
}
