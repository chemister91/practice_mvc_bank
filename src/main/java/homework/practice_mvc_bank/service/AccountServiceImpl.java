package homework.practice_mvc_bank.service;

import homework.practice_mvc_bank.model.Account;
import homework.practice_mvc_bank.model.Client;
import homework.practice_mvc_bank.repos.AccountDao;
import homework.practice_mvc_bank.repos.ClientDao;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {

    private final AccountDao accountDao;
    private final ClientDao clientDao;

    public AccountServiceImpl(AccountDao accountDao, ClientDao clientDao) {
        this.accountDao = accountDao;
        this.clientDao = clientDao;
    }

    @Override
    public List<Account> getAllAccount() {
        return accountDao.getAllAccount();
    }

    @Override
    public Boolean hasAccount(Client client) {
        return getAllAccount()
                .stream()
                .anyMatch(account -> client.equals(account.getClient()));
    }

    @Override
    public AccountService createAccount(String fio, String email, String passHash) throws IllegalAccessException {
        return createAccount(new Client()
                .setFio(fio)
                .setEmail(email)
                .setPassHash(passHash));
    }

    @Override
    public AccountService createAccount(Client client) throws IllegalAccessException {
        accountDao.createAccount(client);
        return this;
    }

    @Override
    public Account getAccountByEmail(String email) {
        Client client = clientDao.getClientByEmail(email);
        return getAllAccount()
                .stream()
                .filter(account -> account.getClient().equals(client))
                .findFirst()
                .orElseGet(Account::new);
    }

    @Override
    public AccountService closeAccount(Integer accountNumber) {
        accountDao.closeAccount(accountNumber);
        return this;
    }

    @Override
    public AccountService openAccount(Integer accountNumber) {
        accountDao.openAccount(accountNumber);
        return this;
    }

    @Override
    public AccountService replenishAccount(Integer accountNumber, Double sum) {
        accountDao.replenishAccount(accountNumber, sum);
        return this;
    }

    @Override
    public AccountService transferMoney(Integer fromAccountNumber, Integer toAccount, Double sum) {
        accountDao.transferMoney(fromAccountNumber, toAccount, sum);
        return this;
    }
}
