package homework.practice_mvc_bank.jms;

import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.jms.ConnectionFactory;
import javax.jms.Message;
import java.util.UUID;

@Component
public class Sender {
    private Logger logger = LogManager.getLogger(Receiver.class);

    private final ConnectionFactory connectionFactory;
    private JmsTemplate jmsTemplate;

    @Autowired
    public Sender(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    @PostConstruct
    public void init() {
        this.jmsTemplate = new JmsTemplate(connectionFactory);
    }

    public void sendMessage(String queueName, Object sending, UUID correlationId) {
        Gson gson = new Gson();
        String messageText = gson.toJson(sending);
        try {
            jmsTemplate.send(queueName, session -> {
                Message message = session.createTextMessage(messageText);
                message.setJMSCorrelationID(correlationId.toString());
                return message;
            });
        } catch (JmsException e) {
            logger.warn("Error on sending message" + messageText, e);
        }
        logger.debug("Sent: %s Queue: %s", messageText, queueName);
    }
}
