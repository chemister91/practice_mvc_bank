package homework.practice_mvc_bank.jms;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import java.util.UUID;

@Component
public class Receiver {
    private Logger logger = LogManager.getLogger(Receiver.class);

    private final ConnectionFactory connectionFactory;
    private JmsTemplate jmsTemplate;

    @Autowired
    public Receiver(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    @PostConstruct
    public void init() {
        this.jmsTemplate = new JmsTemplate(connectionFactory);
    }

    public String receiveMessage(String queueName, UUID correlationId) {
        Message message = jmsTemplate.receiveSelected(queueName, "JMSCorrelationID='" + correlationId + "'");
        TextMessage textMessage = (TextMessage) message;
        try {
            String text = textMessage != null ? textMessage.getText() : Strings.EMPTY;
            logger.debug("Received: {0}. Queue: {1}.", text, queueName);
            return text;
        } catch (JMSException e) {
            logger.warn("Error on receiving message", e);
        }
        return Strings.EMPTY;
    }


}
