package homework.practice_mvc_bank.repos;

import homework.practice_mvc_bank.model.Client;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Repository
public class ClientDaoImpl implements ClientDao {
    private Map<Integer, Client> clients;

    @PostConstruct
    public void init() {
        clients = new HashMap<>();
    }

    @Override
    public Client createClient(Client client) throws IllegalAccessException {
        if (clients.containsValue(client)) {
            throw new IllegalAccessException("Такой пользователь уже существует");
        } else {
            clients.put(client.getClientID(), client);
            return client;
        }
    }

    @Override
    public void deleteClient(Client client) {
        if (client != null && clients.containsValue(client)) {
            clients.remove(client);
        }
    }

    @Override
    public Client getClientByEmail(String email) {
        return clients
                .values()
                .stream()
                .filter(client -> email.equals(client.getEmail()))
                .findFirst()
                .orElseGet(null);
    }
}
