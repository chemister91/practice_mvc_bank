package homework.practice_mvc_bank.repos;

import homework.practice_mvc_bank.model.Account;
import homework.practice_mvc_bank.model.Client;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.*;

@Repository
public class AccountDaoImpl implements AccountDao {
    private Map<Integer, Account> accounts;
    private final ClientDao clientDao;

    public AccountDaoImpl(ClientDao clientDao) {
        this.clientDao = clientDao;
    }

    @PostConstruct
    public void init() {
        accounts = new HashMap<>();
    }

    @Override
    public Account createAccount(Client client) throws IllegalAccessException {
        Account account = new Account();
        account
                .setClient(clientDao.createClient(client))
                .setNumber(account.hashCode())
                .setSum(0.0)
                .setClosed(false);
        accounts.put(account.getNumber(), account);
        return account;
    }

    @Override
    public Account closeAccount(Integer accountNumber) {
        return accounts
                .get(accountNumber)
                .setClosed(true);
    }

    @Override
    public Account openAccount(Integer accountNumber) {
        return accounts
                .get(accountNumber)
                .setClosed(false);
    }

    @Override
    public Account replenishAccount(Integer accountNumber, Double sum) {
        return accounts
                .get(accountNumber)
                .addSum(sum);
    }

    @Override
    public Account transferMoney(Integer fromAccountNumber, Integer toAccount, Double sum) {
        accounts
                .get(toAccount)
                .addSum(sum);
        return accounts
                .get(fromAccountNumber)
                .withdrawSum(sum);
    }

    @Override
    public List<Account> getAllAccount() {
        return new ArrayList<>(accounts.values());
    }
}
