package homework.practice_mvc_bank.repos;

import homework.practice_mvc_bank.model.Client;

public interface ClientDao {
    Client createClient(Client client) throws IllegalAccessException;

    void deleteClient(Client client);

    Client getClientByEmail(String email);
}
