package homework.practice_mvc_bank.repos;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import homework.practice_mvc_bank.jms.Receiver;
import homework.practice_mvc_bank.jms.Sender;
import homework.practice_mvc_bank.model.Account;
import homework.practice_mvc_bank.model.Client;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
@Primary
public class AccountDaoJmsImpl implements AccountDao {
    private static final String ACCOUNT_CREATE_REQUEST = "accountCreate.in";
    private static final String ACCOUNT_CREATE_RESPONSE = "accountCreate.out";
    private static final String ACCOUNT_OPEN_REQUEST = "accountOpen.in";
    private static final String ACCOUNT_OPEN_RESPONSE = "accountOpen.out";
    private static final String ACCOUNT_CLOSE_REQUEST = "accountClose.in";
    private static final String ACCOUNT_CLOSE_RESPONSE = "accountClose.out";
    private static final String ACCOUNT_REPLENISH_REQUEST = "accountReplenish.in";
    private static final String ACCOUNT_REPLENISH_RESPONSE = "accountReplenish.out";
    private static final String ACCOUNT_ALL_REQUEST = "accountAll.in";
    private static final String ACCOUNT_ALL_RESPONSE = "accountAll.out";

    private final Receiver receiver;
    private final Sender sender;

    public AccountDaoJmsImpl(Receiver receiver, Sender sender) {
        this.receiver = receiver;
        this.sender = sender;
    }

    @Override
    public Account createAccount(Client client) throws IllegalAccessException {
        return execute(
                ACCOUNT_CREATE_REQUEST,
                ACCOUNT_CREATE_RESPONSE,
                new Account().setClient(client)
        );
    }

    @Override
    public Account closeAccount(Integer accountNumber) {
        return execute(
                ACCOUNT_CLOSE_REQUEST,
                ACCOUNT_CLOSE_RESPONSE,
                new Account().setNumber(accountNumber)
        );
    }

    @Override
    public Account openAccount(Integer accountNumber) {
        return execute(
                ACCOUNT_OPEN_REQUEST,
                ACCOUNT_OPEN_RESPONSE,
                new Account().setNumber(accountNumber)
        );
    }

    @Override
    public Account replenishAccount(Integer accountNumber, Double sum) {
        return execute(
                ACCOUNT_REPLENISH_REQUEST,
                ACCOUNT_REPLENISH_RESPONSE,
                new Account().setNumber(accountNumber).setSum(sum)
        );
    }

    @Override
    public Account transferMoney(Integer fromAccountNumber, Integer toAccount, Double sum) {
        replenishAccount(toAccount, sum);
        return replenishAccount(fromAccountNumber, -sum);
    }

    @Override
    public List<Account> getAllAccount() {
        return getList(
                ACCOUNT_ALL_REQUEST,
                ACCOUNT_ALL_RESPONSE,
                new Account().setClient(new Client().setRole("ADMIN"))
        );
    }

    private Account execute(String requestQueue, String responseQueue, Account account) {
        UUID correlationID = UUID.randomUUID();
        sender.sendMessage(requestQueue, account, correlationID);
        return new Gson().fromJson(
                receiver.receiveMessage(responseQueue, correlationID), Account.class);
    }

    private List<Account> getList(String requestQueue, String responseQueue, Account account) {
        UUID correlationID = UUID.randomUUID();
        sender.sendMessage(requestQueue, account, correlationID);
        return new Gson().fromJson(
                receiver.receiveMessage(responseQueue, correlationID),
                new TypeToken<List<Account>>(){}.getType());
    }
}
