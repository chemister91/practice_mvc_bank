package homework.practice_mvc_bank.repos;

import homework.practice_mvc_bank.model.Account;
import homework.practice_mvc_bank.model.Client;

import java.util.List;

public interface AccountDao {
    Account createAccount(Client client) throws IllegalAccessException;

    Account closeAccount(Integer accountNumber);

    Account openAccount(Integer accountNumber);

    Account replenishAccount(Integer accountNumber, Double sum);

    Account transferMoney(Integer fromAccountNumber, Integer toAccount, Double sum);

    List<Account> getAllAccount();

}
