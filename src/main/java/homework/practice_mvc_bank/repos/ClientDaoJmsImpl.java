package homework.practice_mvc_bank.repos;

import com.google.gson.Gson;
import homework.practice_mvc_bank.jms.*;
import homework.practice_mvc_bank.model.Client;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
@Primary
public class ClientDaoJmsImpl implements ClientDao {
    private static final String CLIENT_CREATE_REQUEST = "clientCreate.in";
    private static final String CLIENT_CREATE_RESPONSE = "clientCreate.out";
    private static final String CLIENT_DELETE_REQUEST = "clientDelete.in";
    private static final String CLIENT_DELETE_RESPONSE = "clientDelete.out";
    private static final String CLIENT_BY_EMAIL_REQUEST = "clientByEmail.in";
    private static final String CLIENT_BY_EMAIL_RESPONSE = "clientByEmail.out";
    private final Receiver receiver;
    private final Sender sender;

    public ClientDaoJmsImpl(Receiver receiver, Sender sender) {
        this.receiver = receiver;
        this.sender = sender;
    }

    @Override
    public Client createClient(Client client) throws IllegalAccessException {
        return execute(
                CLIENT_CREATE_REQUEST,
                CLIENT_CREATE_RESPONSE,
                client
        );
    }

    @Override
    public void deleteClient(Client client) {
        execute(
                CLIENT_DELETE_REQUEST,
                CLIENT_DELETE_RESPONSE,
                client
        );
    }

    @Override
    public Client getClientByEmail(String email) {
        return execute(
                CLIENT_BY_EMAIL_REQUEST,
                CLIENT_BY_EMAIL_RESPONSE,
                new Client().setEmail(email)
        );
    }

    private Client execute(String requestQueue, String responceQueue, Client client) {
        UUID correlationID = UUID.randomUUID();
        sender.sendMessage(requestQueue, client, correlationID);
        return new Gson().fromJson(
                receiver.receiveMessage(responceQueue, correlationID), Client.class
        );
    }
}
