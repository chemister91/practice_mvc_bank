package homework.practice_mvc_bank.web;

import homework.practice_mvc_bank.model.Account;
import homework.practice_mvc_bank.model.Client;
import homework.practice_mvc_bank.service.AccountService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Controller
public class AccountController {
    private static final String ROLE_ADMIN = "ADMIN";
    private static final String URI_ROOT = "/";
    private static final String URI_LOGIN = "login";
    private static final String URI_REGISTRATION = "registration";
    private static final String URI_ADMIN = "admin/admin";
    private static final String URI_CLIENT = "client/home";
    private static final String URI_ALL_CLIENTS = "admin/clients";
    private static final String URI_OPEN_ACCOUNT = "open-account";
    private static final String URI_CLOSE_ACCOUNT = "close-account";
    private static final String URI_TRANSFER_MONEY = "transfer-money";
    private static final String URI_REPLENISH_ACCOUNT = "replenish-account";
    private static final String URI_WITHDRAW_MONEY = "withdraw-money";

    private final BCryptPasswordEncoder passwordEncoder;
    private final AccountService accountService;

    public AccountController(BCryptPasswordEncoder passwordEncoder, AccountService accountService) {
        this.passwordEncoder = passwordEncoder;
        this.accountService = accountService;
    }

    @PostConstruct
    public void createAdminAccount() throws IllegalAccessException {
        Client admin = new Client()
                .setEmail("admin@myservice.com")
                .setFio("Administrator")
                .setPassHash(passwordEncoder.encode("admin"))
                .setRole(ROLE_ADMIN);
        if (!accountService.hasAccount(admin)) {
            accountService.createAccount(admin);
        }
    }

    @GetMapping({URI_ROOT, URI_ROOT + URI_LOGIN})
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(URI_LOGIN);
        return modelAndView;
    }

    @GetMapping(URI_ROOT + URI_REGISTRATION)
    public ModelAndView registration() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView
                .addObject(new Client())
                .setViewName(URI_REGISTRATION);
        return modelAndView;
    }

    @PostMapping(URI_ROOT + URI_REGISTRATION)
    public ModelAndView createNewClient(@Valid Client client, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();
        if (!bindingResult.hasErrors()) {
            try {
                accountService.createAccount(client.getFio(), client.getEmail(), passwordEncoder.encode(client.getPassHash()));
                modelAndView
                        .addObject("successMessage", "Вы успешно зарегистрированы")
                        .addObject(new Client());
            } catch (IllegalAccessException e) {
                bindingResult.
                        rejectValue("email", "error.client", "Клиент уже зарегистрирован");
            }
        }
        modelAndView.setViewName(URI_REGISTRATION);
        return modelAndView;
    }

    @GetMapping({URI_ROOT + URI_CLIENT, URI_ROOT + URI_ADMIN})
    public ModelAndView home() {
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Account account = accountService.getAccountByEmail(auth.getName());
        modelAndView.addObject("account", account);
        if (auth.getAuthorities().contains(new SimpleGrantedAuthority(ROLE_ADMIN))) {
            modelAndView.setViewName(URI_ADMIN);
        } else {
            modelAndView.setViewName(URI_CLIENT);
        }
        return setClientAttributes(modelAndView);
    }

    @PostMapping({URI_ROOT + URI_ALL_CLIENTS, URI_ROOT + URI_ADMIN})
    public ModelAndView viewAccount(@RequestParam String email) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView
                .addObject(accountService.getAccountByEmail(email))
                .setViewName(URI_CLIENT);
        return setClientAttributes(modelAndView);
    }

    private ModelAndView setClientAttributes(@NotNull ModelAndView modelAndView) {
        Account emptyAccount = new Account().setClient(new Client());
        return modelAndView
                .addObject("emptyAccount", emptyAccount);
    }

    @GetMapping(URI_ROOT + URI_ALL_CLIENTS)
    public ModelAndView getAllAccount() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView
                .addObject("accounts", accountService.getAllAccount())
                .addObject("emptyAccount", new Account().setClient(new Client()))
                .setViewName(URI_ALL_CLIENTS);
        return modelAndView;
    }

    @PostMapping(URI_ROOT + URI_CLOSE_ACCOUNT)
    public ModelAndView closeAccount(@ModelAttribute Account account) {
        accountService.closeAccount(account.getNumber());
        return home();
    }

    @PostMapping(URI_ROOT + URI_OPEN_ACCOUNT)
    public ModelAndView openAccount(@ModelAttribute Account account) {
        accountService.openAccount(account.getNumber());
        return home();
    }

    @PostMapping(URI_ROOT + URI_TRANSFER_MONEY)
    public ModelAndView transferMoney(@ModelAttribute Account account) {
        accountService.transferMoney(
                account.getNumber(),
                accountService.getAccountByEmail(account.getClient().getEmail()).getNumber(),
                account.getSum()
        );
        return home();
    }

    @PostMapping(URI_ROOT + URI_REPLENISH_ACCOUNT)
    public ModelAndView replenishAccount(@ModelAttribute Account account) {
        accountService.replenishAccount(account.getNumber(), account.getSum());
        return home();
    }

    @PostMapping(URI_ROOT + URI_WITHDRAW_MONEY)
    public ModelAndView withdrawMoney(@ModelAttribute Account account) {
        accountService.replenishAccount(account.getNumber(), -account.getSum());
        return home();
    }
}
