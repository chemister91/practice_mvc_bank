package homework.practice_mvc_bank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PracticeMvcBankApplication {

    public static void main(String[] args) {
        SpringApplication.run(PracticeMvcBankApplication.class, args);
    }

}
